title: Simple Fargate project
---
pub_date: 2019-12-02
---
body:

I got asked to showcase a very simple website that shows something like a page and a picture, that can scale, is built on AWS and with large enterprise in mind.

I have created it on https://github.com/ledakis/simplesite-scale

It includes:

1. Docker container for the app itself, which runs nginx and has the content of the site baked into the container.
2. Terraform for the supporting infrastructure the service will run on.
    * VPC/Subnets using the official terraform module.
    * DNS zone + config for my Cloudflare master zone.
    * ECR for the repository the Docker is going to be hosted in.
    * S3 bucket for the application access logs.
    * ACM certificate for the service.
3. Terraform for the service itself. This deploys:
    * The ALB
    * The ECS service (cluster, task definition, service)
    * Scaling policy (number of connections per target from the ALB)
    * IAM roles

This is a WIP. I want to enhance the scaling, maybe to use the lambda that people generally suggest.

Update 4/12/2019:

It appears EKS+Fargate is a thing now and this sounds very, very interesting!

* [AWS Docs on EKS Fargate](https://docs.aws.amazon.com/eks/latest/userguide/fargate.html)
* [Announcement for the EKS Fargate](https://techcrunch.com/2019/12/03/aws-announces-eks-on-fargate-is-available/
)
* [terraform EKS Fargate Profile support](https://github.com/terraform-providers/terraform-provider-aws/blob/v2.41.0/CHANGELOG.md)

For v2.0 of this project, I will plan to work on the following:

1. Convert the task definition to the k8s manifest and try to have terraform deploy it. This is only to be a test, as it will probably involve spinning up an EKS cluster and along with that, the cost for it. (this is just to showcase some terraforming + aws + architecture, so I try to keep costs low as I test it on a personal account)
2. Change the egress to S3 (for ALB logs and docker layers) and to ECS api to go via a [VPC Endpoint](https://www.terraform.io/docs/providers/aws/r/vpc_endpoint.html) so that we remove the need for a NAT Gateway in the VPC. Remember: Principle of least priviledge. The traffic to AWS services should not go through the internet but via a forced path by us.
3. I will move the certificate creation to the service terraform directory, so then the service can be ran many times independently of the main infrastructure bit.